# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github systemd-service autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Box Backup is an open source, completely automatic, secure, encrypted on-line backup system"

LICENCES="
    BSD-3    [[ note = [ For some libs and tests ] ]]
    GPL-2    [[ note = [ There is an additional linking exception ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-libs/libxslt [[ note = [ For xsltproc ] ]]
    build+run:
        sys-libs/db:=
        sys-libs/readline
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=0.9.7] )
        user/boxbackup
"

# parallel builds are broken
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

# tests want to access /dev/log
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-static-bin' )

AT_M4DIR=( infrastructure/m4 )

# The install script is a bit broken... Just copy the files manually.
src_install() {
    # Client binaries
    dobin release/bin/{bbackupctl/bbackupctl,bbackupd/bbackupd,bbackupquery/bbackupquery}
    dobin bin/bbackupd/bbackupd-config

    # Server binaries
    dobin release/bin/{bbstoreaccounts/bbstoreaccounts,bbstored/bbstored}
    dobin bin/bbstored/{bbstored-certs,bbstored-config}
    dobin lib/raidfile/raidfile-config

    # Documentation
    dodoc docs/htmlguide/man-html/*
    doman docs/man/*
    emagicdocs

    # Systemd files
    install_systemd_files
}

