# Copyright 2014 Niels Ole Salscheider
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="C++ library for modeling and solving large complicated nonlinear least squares problems"

HOMEPAGE="http://ceres-solver.org"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

LICENCES="
    !eigen-sparse? ( BSD-3 )
    eigen-sparse? ( LGPL-2.1 )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
    eigen-sparse [[ description = [ Use Eigen for Cholesky factorization. This forces the LGPL licence ] ]]
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    ( providers: OpenBLAS blas ) [[
        *description = [ Basic Linear Algebra Subroutines provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        doc? (
            dev-python/Sphinx
            dev-python/sphinx_rtd_theme
        )
    build+run:
        sci-libs/eigen:3[>=3.3.4]
        sci-libs/lapack
        sci-libs/SuiteSparse
        providers:OpenBLAS? ( sci-libs/OpenBLAS )
        providers:blas? ( sci-libs/blas )
"

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc BUILD_DOCUMENTATION'
    'eigen-sparse EIGENSPARSE'
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_BENCHMARKS:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCERES_DOCS_INSTALL_DIR:PATH=/usr/share/doc/${PNVR}
    -DCUSTOM_BLAS:BOOL=TRUE
    -DLAPACK:BOOL=TRUE
    -DLIB_SUFFIX:STRING=""
    -DSCHUR_SPECIALIZATIONS:BOOL=TRUE
    -DUSE_CUDA:BOOL=FALSE
    # SuiteSparse is strongly recommended for bundle adjustment
    -DSUITESPARSE:BOOL=TRUE

    # unpackaged dependencies.
    -DBUILD_TESTING:BOOL=FALSE
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DGFLAGS:BOOL=FALSE

    # Slower alternative to SuiteSparse
    -DCXSPARSE:BOOL=FALSE

    -DMINIGLOG:BOOL=TRUE
)

