# Copyright 2023 Ole Salscheider <ole@salscheider.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=RadeonOpenCompute tag=rocm-${PV} ] cmake [ api=2 ]

SUMMARY="Radeon Open Compute Device Libs"

LICENCES="UoI-NCSA"
SLOT="0"
PLATFORMS="~amd64"

LLVM_SLOT="17"

DEPENDENCIES="
    build+run:
        dev-lang/clang:${LLVM_SLOT}
        dev-lang/llvm:${LLVM_SLOT}
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLLVM_DIR:PATH=/usr/$(exhost --target)/lib/llvm/${LLVM_SLOT}/lib/cmake/llvm
    -DClang_DIR:PATH=/usr/$(exhost --target)/lib/llvm/${LLVM_SLOT}/lib/cmake/clang
)

# Some tests fail with upstream clang for now
RESTRICT="test"

src_prepare() {
    edo sed -e "s:amdgcn/bitcode:lib/llvm/${LLVM_SLOT}/amdgcn/bitcode:" -i "${CMAKE_SOURCE}"/cmake/OCL.cmake
    edo sed -e "s:amdgcn/bitcode:lib/llvm/${LLVM_SLOT}/amdgcn/bitcode:" -i "${CMAKE_SOURCE}"/cmake/Packages.cmake

    default
}

